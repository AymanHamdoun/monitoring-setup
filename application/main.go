package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

var (
	requestDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_request_duration_milliseconds",
		Help: "Time taken for requests",
	}, []string{"path"})
)

// Simple app that logs the paths and params a request sends
// and reports to prometheus the timings it took
func main() {
	prometheus.MustRegister(requestDuration)

	logger, _ := zap.NewProduction()

	router := gin.Default()

	router.Use(func(c *gin.Context) {
		path := c.Request.URL.Path

		start := time.Now()
		c.Next()
		duration := time.Since(start)

		requestDuration.With(prometheus.Labels{"path": path}).Observe(float64(duration.Milliseconds()))
		logger.Info("Request processed",
			zap.String("path", path),
			zap.String("parameters", c.Request.URL.RawQuery),
		)
	})

	router.GET("/metrics", gin.WrapH(promhttp.Handler()))

	router.GET("/:path", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Hello, World!",
		})
	})

	err := router.Run(":80")
	if err != nil {
		logger.Fatal("Failed to run server", zap.Error(err))
	}
}
